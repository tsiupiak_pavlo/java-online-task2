import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        final int counts = 100;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter possible number of guests (min 3): ");
        Random random = new Random();
        int n = scan.nextInt();
        int countTimes = 0;
        int peopleReached = 0;
        for (int i = 0; i < counts; i++) {
            boolean[] people = new boolean[n];
            people[1] = true;
            boolean heard = false;
            int nextPeople;
            int thisPeople = 1;
            while (!heard) {
                nextPeople = 1 + random.nextInt(n - 1);
                if (nextPeople == thisPeople) {
                    while (nextPeople == thisPeople) {
                        nextPeople = 1 + random.nextInt(n - 1);
                    }
                }
                if (people[nextPeople]) {
                    if (rumorSpreaded(people)) {
                        countTimes++;
                    }
                    peopleReached = peopleReached + countPeopleReached(people);
                    heard = true;
                }
                people[nextPeople] = true;
                thisPeople = nextPeople;
            }
        }

        System.out.println("The probability that everyone will hear rumor except Alice in " + counts + " attempts: " +
                (double) countTimes / counts);
        System.out.println("Number of people that rumor reached is: " + peopleReached / counts);
    }

    private static int countPeopleReached(boolean[] arr) {
        int counter = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i]) {
                counter++;
            }
        }
        return counter;
    }

    private static boolean rumorSpreaded(boolean[] arr) {
        for (int i = 1; i < arr.length; i++) {
            if (!arr[i]) {
                return false;
            }
        }
        return true;
    }

}
